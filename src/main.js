import { createApp } from 'vue'
import App from './App.vue'
import './index.css'
import Router from './router'
import 'ant-design-vue/dist/antd.css'
import Ant from 'ant-design-vue'

createApp(App).use(Router).use(Ant).mount('#app')
