import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import routes from './routes'
console.log(routes);

const Router = createRouter({
    history: createWebHistory(),
    routes
})

Router.beforeEach((to, from, next) => {
    if (to.meta.title) {
        document.title = to.meta.title
    }
    next()  //拦截之后必须要next()
})

// const originalPush = Router.prototype.push;
// Router.prototype.push = function push(location) {
//     return originalPush.call(this, location).catch(err => err)
// }

// const originalReplace = Router.prototype.replace;
// Router.prototype.replace = function replace(location) {
//     return originalPush.call(this, location).catch(err => err)
// }
export default Router