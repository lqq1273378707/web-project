export default [
    {
        path: '/home',
        name: 'home',
        component: () => import('../views/home/index.vue'),
        meta: {
            title: '首页'
        }
    },
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/error',
        name: 'error',
        component: () => import('../views/error/404.vue'),
        meta: {
            title: 'Error For 404'
        }
    },
    {
        path: '/:catchAll(.*)',  //vite搭建项目，匹配所有错误页面需要这样
        redirect: '/error'
    }
]